class RegistrationError extends Error {
    constructor(message) {
        super(message);
        this.name = 'RegistrationError';
        this.statusCode = 400;
    }
}
module.exports = RegistrationError;
