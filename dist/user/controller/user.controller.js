"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../model/user.model');
const RegistrationError = require('../Errors/registration.error');
const AuthenticationError = require('../Errors/authentication.error');
const DatabaseError = require('../Errors/database.error');
const { ConfigService } = require('../../../config');
const configService = new ConfigService();
// User Registration
function register(userDto) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const { username, password } = userDto;
            // Check if the username already exists
            const existingUser = yield User.findOne({ username });
            if (existingUser) {
                return new RegistrationError('Username already exists');
            }
            // Hash the password
            const salt = yield bcrypt.genSalt(10);
            const hashedPassword = yield bcrypt.hash(password, salt);
            // Create a new user
            const user = new User({ username, password: hashedPassword });
            yield user.save();
            return { message: 'User registered successfully', statusCode: 201 };
        }
        catch (error) {
            console.error(error);
            return new DatabaseError('Internal Server Error');
        }
    });
}
// User Login
function login(userDto) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const { username, password } = userDto;
            // Check if the user exists
            const user = yield User.findOne({ username });
            if (!user) {
                return new AuthenticationError('Invalid username or password');
            }
            // Check if the password is correct
            const isPasswordValid = yield bcrypt.compare(password, user.password);
            if (!isPasswordValid) {
                return new AuthenticationError('Invalid username or password');
            }
            // Create and return a JWT token
            const token = jwt.sign({ userId: user._id }, 'your-secret-key');
            return {
                statusCode: 200,
                message: 'User successfully authenticated',
                token,
                user: {
                    _id: user._id,
                    username: user.username,
                    orders: user.orders
                }
            };
        }
        catch (error) {
            console.error(error);
            return new DatabaseError(error);
        }
    });
}
module.exports = { register, login };
