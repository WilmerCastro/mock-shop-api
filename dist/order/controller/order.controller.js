"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios = require("axios");
const BadRequestError = require('../Errors/bad_request.error');
const NotFoundError = require('../Errors/not_found.error');
const Order = require('../model/order.model');
const User = require('../../user/model/user.model');
const DatabaseError = require("../../user/Errors/database.error");
const { OrderStatus } = require("../utils/enum/order_status.enum");
const CallApiError = require("../Errors/call_api.error");
function getAllProduct() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const response = yield axios.get("https://fakestoreapi.com/products");
            const data = response.data;
            return { data: data, statusCode: 200 };
        }
        catch (error) {
            console.error(error);
            return new CallApiError(error);
        }
    });
}
function getProductById(id) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const response = yield axios.get(`https://fakestoreapi.com/products/${id}`);
            const data = response.data;
            return { data: data, statusCode: 200 };
        }
        catch (error) {
            console.error(error);
            return new CallApiError(error);
        }
    });
}
function createOrder(createOrderDto) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const { productList, date, status, user } = createOrderDto;
            const foundedUser = yield User.findById(user);
            if (!foundedUser) {
                return new NotFoundError('Invalid username or password');
            }
            let subTotal = 0;
            if (!productList) {
                return new BadRequestError('productList cannot be null');
            }
            for (const productListElement of productList) {
                let product = yield getProductById(productListElement.id);
                if (!(product instanceof CallApiError)) {
                    subTotal += product.data.price;
                }
            }
            let interest = subTotal * 0.15;
            let totalValue = subTotal + interest;
            const order = new Order({ productList, subTotal, interest, totalValue, date, status, user });
            yield order.save();
            return { message: 'Order added', statusCode: 201 };
        }
        catch (error) {
            console.error(error);
            return new DatabaseError('Internal Server Error');
        }
    });
}
function getAllOrderByUserId(user) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const orders = yield Order.find({ user });
            return { data: orders[0], statusCode: 200 };
        }
        catch (error) {
            console.error(error);
            return new DatabaseError('Internal Server Error');
        }
    });
}
function getAllActiveOrder(user) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const orders = yield Order.find({ user, status: OrderStatus.ACTIVE });
            return { data: orders[0], statusCode: 200 };
        }
        catch (error) {
            console.error(error);
            return new DatabaseError('Internal Server Error');
        }
    });
}
function getOrderById(orderId) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const order = yield Order.findById(orderId);
            return { data: order, statusCode: 200 };
        }
        catch (error) {
            console.error(error);
            return new DatabaseError('Internal Server Error');
        }
    });
}
function changeAllActiveOrdersToCompleted(user) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            // Find all active orders by the user
            const activeOrders = yield Order.find({ user, status: OrderStatus.ACTIVE });
            // Update each active order to completed
            const updatePromises = activeOrders.map((order) => __awaiter(this, void 0, void 0, function* () {
                order.status = OrderStatus.COMPLETED;
                return order.save();
            }));
            // Wait for all the updates to complete
            yield Promise.all(updatePromises);
            return { message: 'All active orders changed to completed', statusCode: 200 };
        }
        catch (error) {
            console.error(error);
            return { message: 'Internal Server Error', statusCode: 500 };
        }
    });
}
function updateOrder(updateOrderDto) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const { productList, orderId } = updateOrderDto;
            // if (status === OrderStatus.COMPLETED) {
            //     return {message: 'This Order is Completed, cannot updated', statusCode: 403}
            // }
            const order = yield Order.findById(orderId);
            if (!order) {
                return new NotFoundError('order not found');
            }
            let subTotal = order.subTotal;
            productList.push(order.productList);
            for (const productListElement of productList) {
                let product = yield getProductById(productListElement.id);
                subTotal += product.data.price;
            }
            let interest = subTotal * 0.15;
            let totalValue = subTotal + interest;
            const updateOrder = new Order({ productList, subTotal, interest, totalValue });
            yield order.updateOne({ _id: orderId }, { updateOrder });
            return { message: 'Order updated', statusCode: 200 };
        }
        catch (error) {
            console.error(error);
            return new DatabaseError('Internal Server Error');
        }
    });
}
// async function updateOrder(updateOrderDto: UpdateOrderDto): Promise<any> {
//     try {
//         const {productList, orderId} = updateOrderDto;
//         // if (status === OrderStatus.COMPLETED) {
//         //     return {message: 'This Order is Completed, cannot updated', statusCode: 403}
//         // }
//
//         const order = await Order.findById(orderId);
//
//         if (!order) {
//             return new NotFoundError('order not found')
//         }
//         let subTotal: number = order.subTotal;
//
//         for (const productListElement of productList) {
//             let product = await getProductById(productListElement.id);
//
//             order.productList.push(product);
//             subTotal += product.data.price;
//         }
//         let interest: number = subTotal * 0.15;
//         let totalValue: number = subTotal + interest;
//
//         order.subTotal = subTotal;
//         order.interest = interest;
//         order.totalValue = totalValue;
//
//         await order.save();
//         return {message: 'Order updated', statusCode: 200}
//     } catch (error) {
//         console.error(error);
//         return new DatabaseError('Internal Server Error');
//     }
// }
function createOrUpdate(createOrderDto) {
    return __awaiter(this, void 0, void 0, function* () {
        const order = yield getAllActiveOrder(createOrderDto.user);
        if (!(order === null || order === void 0 ? void 0 : order.data)) {
            return yield createOrder(createOrderDto);
        }
        return yield updateOrder(createOrderDto);
    });
}
module.exports = {
    getAllProduct,
    getOrderById,
    getAllActiveOrder,
    getAllOrderByUserId,
    updateOrder,
    getProductById,
    createOrUpdate,
    changeAllActiveOrdersToCompleted
};
