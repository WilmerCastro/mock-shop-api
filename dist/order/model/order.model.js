"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const { OrderStatus } = require("../utils/enum/order_status.enum");
const orderSchema = new mongoose_1.default.Schema({
    productList: {
        type: [
            {
                id: {
                    type: Number,
                    required: true
                },
                title: {
                    type: String,
                    required: true
                },
                price: {
                    type: Number,
                    required: true
                },
                description: {
                    type: String,
                    required: true
                },
                category: {
                    type: String,
                    required: true
                },
                image: {
                    type: String,
                    required: true
                },
                rating: {
                    rate: {
                        type: Number,
                        required: true
                    },
                    count: {
                        type: Number,
                        required: true
                    }
                }
            }
        ],
        required: true
    },
    subTotal: {
        type: Number,
        required: true
    },
    interest: {
        type: Number,
        required: true
    },
    totalValue: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    status: {
        type: String,
        default: OrderStatus.ACTIVE
    },
    user: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'User'
    }
});
const Order = mongoose_1.default.model('Order', orderSchema);
module.exports = Order;
