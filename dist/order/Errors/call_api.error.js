class CallApiError extends Error {
    constructor(message) {
        super(message);
        this.name = 'ApiError';
        this.statusCode = 444;
    }
}
module.exports = CallApiError;
