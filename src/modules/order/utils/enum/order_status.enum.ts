export enum OrderStatus {
    ACTIVE = "active",
    COMPLETED = "completed"
}
